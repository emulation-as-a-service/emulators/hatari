FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="hatari"
LABEL "EAAS_EMULATOR_VERSION"="test-0.1"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
hatari

ADD metadata /metadata
